/**
 * 
 */
package seleniumBasics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author TOSHIBA
 *
 */
public class scenario1 {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "E:\\ROGER\\Linda_Roger\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.ebay.com/");
		driver.manage().window().maximize();
		String title=driver.getTitle();
		System.out.println("Title of current page is "+title);
		String url=driver.getCurrentUrl();
		System.out.println("URL "+url);
		driver.navigate().to("https://www.flipkart.com/");
		Thread.sleep(3000);
		String title1=driver.getTitle();
		System.out.println("Title of current page is "+title1);
		Thread.sleep(3000);
		driver.navigate().to("https://www.amazon.in/");
		String title2=driver.getTitle();
		System.out.println("Title of current page is "+title2);
		Thread.sleep(3000);
		driver.navigate().back();
		Thread.sleep(3000);
		String url1=driver.getCurrentUrl();
		System.out.println("URL "+url1);
		Thread.sleep(3000);
		driver.navigate().forward();
		Thread.sleep(3000);
		String url2=driver.getCurrentUrl();
		System.out.println("URL "+url2);
		Thread.sleep(3000);
		driver.navigate().back();
		Thread.sleep(3000);
		driver.navigate().back();
		Thread.sleep(3000);
		String url3=driver.getCurrentUrl();
		System.out.println("URL "+url3);
		driver.close();
		
	}

}
