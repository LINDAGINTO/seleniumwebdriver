/**
 * 
 */
package seleniumBasics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author TOSHIBA
 *
 */
public class KTUTrial {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "E:\\ROGER\\Linda_Roger\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://ktu.edu.in/home.htm;jsessionid=8FD8D3DE532AD02B3156B9308BC1895E.KTUApp1");
		driver.findElement(By.xpath("//a[@id='academicAudit']")).click();
		driver.findElement(By.xpath("//label[text()='Course Diary']//following::a")).click();
		Thread.sleep(4000);
		driver.close();
		
	}

}
