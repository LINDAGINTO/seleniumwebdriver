/**
 * 
 */
package seleniumBasics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author TOSHIBA
 *
 */
public class Guru99 {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
	
		System.setProperty("webdriver.chrome.driver", "E:\\ROGER\\Linda_Roger\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/v4/");
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr176687");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("EnEtequ");
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		driver.findElement(By.xpath("//a[text()='New Customer']")).click();
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys("Linda Mol Ouseph");
		driver.findElement(By.xpath("//input[@name='rad1' and @value='f']")).click();
		driver.findElement(By.xpath("//input[@id='dob']")).sendKeys("19SEP1986");
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys("KUTTARATHIL CHIRA");
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys("ALLEPPEY");
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys("KERALA");
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys("688525");
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys("8129460896");
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys("lindamolouseph@gmail.com");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("1234");
		driver.findElement(By.xpath("//input[@name='sub']")).click();
		Thread.sleep(4000);
		driver.close();
		
	}

}
