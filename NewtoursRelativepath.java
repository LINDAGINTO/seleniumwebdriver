/**
 * 
 */
package seleniumBasics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author TOSHIBA
 *
 */
public class NewtoursRelativepath {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "E:\\ROGER\\Linda_Roger\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		driver.findElement(By.xpath("//a[text()='SIGN-ON']")).click();
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("invalid");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("invalid");
		driver.findElement(By.xpath("//input[@name='login']")).click();
		Thread.sleep(4000);
		driver.close();
		
	}

}
