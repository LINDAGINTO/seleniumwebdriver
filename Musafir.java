/**
 * 
 */
package seleniumBasics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author TOSHIBA
 *
 */
public class Musafir {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "E:\\ROGER\\Linda_Roger\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://in.musafir.com/");
		driver.findElement(By.xpath("//input[@name='Origin']")).sendKeys("Kochi, India (COK)");
		driver.findElement(By.xpath("//input[@name='Destination']")).sendKeys("Thiruvananthapuram, India (TRV)");
		driver.findElement(By.xpath("//input[@name='StartDate']")).click();
		WebDriverWait wait = new WebDriverWait(driver,20);
		WebElement strt_date;
		strt_date = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@date='2019-02-20']")));
		strt_date.click();
		
		
		//driver.findElement(By.xpath("//li[@date='2019-02-20']")).click();
		driver.findElement(By.xpath("//input[@name='EndDate']")).click();
		
		WebDriverWait wait1 = new WebDriverWait(driver,20);
		WebElement strt_date1;
		strt_date1 = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@date='2019-02-23']")));
		strt_date1.click();
		
		//driver.findElement(By.xpath("//li[@date='2019-02-23']")).click();
		/*WebElement AdultsFlight = driver.findElement(By.xpath("//select[@name='AdultsFlight']"));
		Select s1 = new Select(AdultsFlight);
		s1.selectByValue("2");
		
		WebElement ChildrenFlight = driver.findElement(By.xpath("//select[@name='ChildrenFlight']"));
		Select s2 = new Select(ChildrenFlight);
		s2.selectByIndex(1);
		
		WebElement InfantsFlight = driver.findElement(By.xpath("//select[@name='InfantsFlight']"));
		Select s3 = new Select(InfantsFlight);
		s3.selectByVisibleText("1 infant");
		
		*/
		driver.findElement(By.xpath("//i[text()='Find flights']")).click();
		driver.findElement(By.xpath("//i[text()='Book']")).click();
		driver.findElement(By.xpath("//option[text()='Mrs']")).click();
		driver.findElement(By.xpath("//input[@placeholder='First name']")).click();
		driver.findElement(By.xpath("//input[@placeholder='First name']")).sendKeys("Linda");
		driver.findElement(By.xpath("//input[@placeholder='Middle name (optional)']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Middle name (optional)']")).sendKeys("Mol");
		driver.findElement(By.xpath("//input[@placeholder='Last name']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Last name']")).sendKeys("Ouseph");
		driver.findElement(By.xpath("//input[@placeholder='Date of birth (dd/mm/yyyy)']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Date of birth (dd/mm/yyyy)']")).sendKeys("19/09/1986");
		driver.findElement(By.xpath("//input[@placeholder='Email address']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Email address']")).sendKeys("lindamolouseph@gmail.com");
		driver.findElement(By.xpath("//input[@placeholder='Mobile']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Mobile']")).sendKeys("8129460896");
		//driver.findElement(By.xpath("//input[@id='chkGstOptIn']")).click();
		driver.findElement(By.xpath("//a[@id='btnContinue']")).click();
		
		Thread.sleep(4000);
		driver.close();
	}

}
